package com.example.parcial1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends ArrayAdapter<Contacto> {



    private static class ViewHolder{
        TextView nombre;
        TextView telefono;
        TextView tipo;
    }

    public MyAdapter(Context context, ArrayList<Contacto> contacto){
        super(context,R.layout.lista,contacto);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Contacto contacto = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lista, parent, false);
            viewHolder.nombre = (TextView) convertView.findViewById(R.id.txtNombreLista);
            viewHolder.telefono = (TextView) convertView.findViewById(R.id.txtTelefonoLista);
            viewHolder.tipo = (TextView) convertView.findViewById(R.id.txtTipoLista);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.nombre.setText(contacto.getNombre());
        viewHolder.telefono.setText(contacto.getTelefono());
        viewHolder.tipo.setText(contacto.getTipo());
        return convertView;
    }
}

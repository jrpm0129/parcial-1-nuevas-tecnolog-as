package com.example.parcial1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Mostrar extends AppCompatActivity {

    ListView lista;
    ArrayList<Contacto> contacto = new ArrayList<Contacto>();
    MyAdapter arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar);

        Intent i = getIntent();
        try{
            if(i.getParcelableArrayListExtra("list")!= null){
                contacto = i.getParcelableArrayListExtra("list");
            }else{
                Toast.makeText(getApplicationContext(),"No tiene datos", Toast.LENGTH_SHORT).show();
            }

        }catch (Exception e){
            System.out.println(e);
        }

        if(contacto.size()==0){
            Toast.makeText(getApplicationContext(),"Sin Datos", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getApplicationContext(),"Datos Recibidos", Toast.LENGTH_SHORT).show();
            lista = findViewById(R.id.lstLista);
            arrayAdapter = new MyAdapter(this, contacto);
            lista.setAdapter(arrayAdapter);
            arrayAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_page, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.optionHome) {
            Intent i = new Intent(getApplicationContext(),HomePage.class);
            i.putExtra("list", contacto);
            startActivity(i);
        }
        if (id == R.id.optionGrabar) {
            Intent i = new Intent(getApplicationContext(),GrabarContacto.class);
            i.putExtra("list", contacto);
            startActivity(i);
        }
        if (id == R.id.optionMostrar) {
            Intent i = new Intent(getApplicationContext(),Mostrar.class);
            i.putExtra("list", contacto);
            startActivity(i);
        }
        if (id == R.id.optionMostrarMayuscula) {
            Intent i = new Intent(getApplicationContext(),MostrarMayuscula.class);
            i.putExtra("list", contacto);
            startActivity(i);
        }
        if (id == R.id.optionMostrarNumero) {
            Intent i = new Intent(getApplicationContext(),MostrarNumeros.class);
            i.putExtra("list", contacto);
            startActivity(i);
        }
        if(id == R.id.optionAleatorio){
            Intent i = new Intent(getApplicationContext(),NumerosAleatorios.class);
            i.putExtra("list", contacto);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
}

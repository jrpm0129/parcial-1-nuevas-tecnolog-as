package com.example.parcial1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class NumerosAleatorios extends AppCompatActivity {

    ArrayList<Contacto> c = new ArrayList<Contacto>();
    ListView lista;
    ArrayList<String> numeros = new ArrayList<String>();
    ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numeros_aleatorios);

        Intent i = getIntent();
        try{
            if(i.getParcelableArrayListExtra("list")!= null){
                c = i.getParcelableArrayListExtra("list");
            }else{
                Toast.makeText(getApplicationContext(),"No tiene datos", Toast.LENGTH_SHORT).show();
            }

        }catch (Exception e){
            System.out.println(e);
        }

        lista = findViewById(R.id.lstNumerosAleatorios);
        numeros = numeros();
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, numeros);
        lista.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
    }

    public ArrayList<String> numeros(){
        ArrayList<String> num = new ArrayList<String>();
        int numero;
        String [] pre = {"300","310","320"};
        int n=10;
        String numeroFinal = "";
        String valorA = "";
        int valor=0;
        for(int i=0;i<10;i++){
            numero = (int) Math.round(Math.random()*2);
            for(int j =0;j<7;j++){
                valor = (int) (Math.random()*n);
                valorA = valorA+""+valor;
            }

            numeroFinal = pre[numero] +valorA;
            num.add(numeroFinal);
            valorA = "";
        }
        return num;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_page, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.optionHome) {
            Intent i = new Intent(getApplicationContext(),HomePage.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionGrabar) {
            Intent i = new Intent(getApplicationContext(),GrabarContacto.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionMostrar) {
            Intent i = new Intent(getApplicationContext(),Mostrar.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionMostrarMayuscula) {
            Intent i = new Intent(getApplicationContext(),MostrarMayuscula.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionMostrarNumero) {
            Intent i = new Intent(getApplicationContext(),MostrarNumeros.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if(id == R.id.optionAleatorio){
            Intent i = new Intent(getApplicationContext(),NumerosAleatorios.class);
            i.putExtra("list", c);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
}

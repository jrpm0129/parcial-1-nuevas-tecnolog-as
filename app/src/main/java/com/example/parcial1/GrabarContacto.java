package com.example.parcial1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class GrabarContacto extends AppCompatActivity {

    Button cancelar, grabar;
    EditText nombre, telefono;
    RadioGroup tipo;
    RadioButton familia, trabajo, amigos, ocacional;

    ArrayList<Contacto> c = new ArrayList<Contacto>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grabar_contacto);


        Intent i = getIntent();
        try{
            if(i.getParcelableArrayListExtra("list")!= null){
                c = i.getParcelableArrayListExtra("list");
            }else{
                Toast.makeText(getApplicationContext(),"No tiene datos", Toast.LENGTH_SHORT).show();
            }

        }catch (Exception e){
            System.out.println(e);
        }



        nombre = (EditText) findViewById(R.id.editTextNombre);
        telefono = (EditText) findViewById(R.id.editTextTelefono);
        tipo= (RadioGroup) findViewById(R.id.radioGroupTipo);

        familia = (RadioButton) findViewById(R.id.rbtnFamilia);
        trabajo = (RadioButton) findViewById(R.id.rbtnTrabajo);
        amigos = (RadioButton) findViewById(R.id.rbtnAmigos);
        ocacional = (RadioButton) findViewById(R.id.rbtnOcacional);

        cancelar = findViewById(R.id.btnCancelar);
        grabar = findViewById(R.id.btnGrabar);

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombre.setText("");
                telefono.setText("");
                tipo.clearCheck();
            }});

        grabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(nombre.getText()) || TextUtils.isEmpty(telefono.getText()) || !Chequeado(familia,trabajo,amigos,ocacional) ){
                    Toast.makeText(getApplicationContext(),"Datos Incompletos", Toast.LENGTH_SHORT).show();
                }else{
                    if(!TextUtils.isEmpty(nombre.getText()) && !TextUtils.isEmpty(telefono.getText()) && Chequeado(familia,trabajo,amigos,ocacional)){
                        c.add(new Contacto(nombre.getText().toString(),telefono.getText().toString(),Tipo(familia,trabajo,amigos,ocacional)));
                        Toast.makeText(getApplicationContext(),"Contacto Guardado", Toast.LENGTH_SHORT).show();
                        nombre.setText("");
                        telefono.setText("");
                        tipo.clearCheck();
                    }
                }
            }});
    }

    public String Tipo(RadioButton familia, RadioButton trabajo, RadioButton amigos, RadioButton ocacional){
        String dato="";
        if(familia.isChecked()){
            dato = "Familia";
        }
        if(trabajo.isChecked()){
            dato = "Trabajo";
        }
        if(amigos.isChecked()){
            dato = "Amigos";
        }
        if(ocacional.isChecked()){
            dato = ocacional.getText().toString();
        }
        return dato;
    }
    public boolean Chequeado(RadioButton familia, RadioButton trabajo, RadioButton amigos, RadioButton ocacional){
        boolean dato=false;
        if(familia.isChecked()){
            dato = true;
        }
        if(trabajo.isChecked()){
            dato = true;
        }
        if(amigos.isChecked()){
            dato = true;
        }
        if(ocacional.isChecked()){
            dato = true;
        }
        return dato;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_page, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.optionHome) {
            Intent i = new Intent(getApplicationContext(),HomePage.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionGrabar) {
            Intent i = new Intent(getApplicationContext(),GrabarContacto.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionMostrar) {
            Intent i = new Intent(getApplicationContext(),Mostrar.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionMostrarMayuscula) {
            Intent i = new Intent(getApplicationContext(),MostrarMayuscula.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionMostrarNumero) {
            Intent i = new Intent(getApplicationContext(),MostrarNumeros.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if(id == R.id.optionAleatorio){
            Intent i = new Intent(getApplicationContext(),NumerosAleatorios.class);
            i.putExtra("list", c);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.example.parcial1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MostrarNumeros extends AppCompatActivity {

    ArrayList<Contacto> c = new ArrayList<Contacto>();
    MyAdapter arrayAdapter;
    ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_numeros);

        Intent i = getIntent();
        try{
            if(i.getParcelableArrayListExtra("list")!= null){
                c = i.getParcelableArrayListExtra("list");
            }else{
                Toast.makeText(getApplicationContext(),"No tiene datos", Toast.LENGTH_SHORT).show();
            }

        }catch (Exception e){
            System.out.println(e);
        }

        if(c.size()==0){
            Toast.makeText(getApplicationContext(),"Sin Datos", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getApplicationContext(),"Datos Recibidos", Toast.LENGTH_SHORT).show();
            lista = findViewById(R.id.lstListaNumeros);
            ArrayList<Contacto> contacto = Numeros(c);
            if(contacto.size()==0){
                Toast.makeText(getApplicationContext(),"No se encontraron Numeros que comienzan en 300, 310 y 320", Toast.LENGTH_SHORT).show();
            }else if(contacto.size()!=0){
                arrayAdapter = new MyAdapter(this, contacto);
                lista.setAdapter(arrayAdapter);
                arrayAdapter.notifyDataSetChanged();
            }

        }
    }

    private ArrayList<Contacto> Numeros(ArrayList<Contacto> c) {
        ArrayList<Contacto> contacto = new ArrayList<Contacto>();

        for(int i=0;i<c.size();i++){
            if(c.get(i).getTelefono().startsWith("300") || c.get(i).getTelefono().startsWith("310") || c.get(i).getTelefono().startsWith("320")){
                contacto.add(new Contacto(c.get(i).getNombre(),c.get(i).getTelefono(),c.get(i).getTipo()));
            }
        }

        return contacto;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_page, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.optionHome) {
            Intent i = new Intent(getApplicationContext(),HomePage.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionGrabar) {
            Intent i = new Intent(getApplicationContext(),GrabarContacto.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionMostrar) {
            Intent i = new Intent(getApplicationContext(),Mostrar.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionMostrarMayuscula) {
            Intent i = new Intent(getApplicationContext(),MostrarMayuscula.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if (id == R.id.optionMostrarNumero) {
            Intent i = new Intent(getApplicationContext(),MostrarNumeros.class);
            i.putExtra("list", c);
            startActivity(i);
        }
        if(id == R.id.optionAleatorio){
            Intent i = new Intent(getApplicationContext(),NumerosAleatorios.class);
            i.putExtra("list", c);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
}
